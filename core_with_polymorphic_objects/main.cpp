#include <util.hpp>

struct A
{
    A(int first, int second)
        : first_(first)
        , second_(second)
    {
    }

    virtual int sum()
    {
        return first_ + second_;
    }

    int first_;
    int second_;
};

int main()
{

    for (int i = 0; i < 100; i++)
    {
        volatile A * a = new A{i, i};
    }
    make_core();
}