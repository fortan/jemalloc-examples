#include <jemalloc/jemalloc.h>
#include <unistd.h>
#include <memory.h>
#include <stdio.h>

int main()
{

    int *a = (int*)malloc(sizeof(int));
    int *b = (int*)0xf00000f0f0f0f;
    int *c = (int*)0xf1;
    //*a = 10;
    printf("%i %i\n", malloc_usable_size(a), malloc_usable_size(c));
}