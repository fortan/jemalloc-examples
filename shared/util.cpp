#include "util.hpp"

void make_core()
{
    using namespace std;
    const auto pid = ::getpid();
    system((string("gcore ") + std::to_string(pid)).c_str());
}