#include <iostream>
#include <jemalloc/jemalloc.h>
#include <unistd.h>
#include <string>
#include <cstdlib>
#include <thread>
#include <vector>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <iomanip>

std::string to_string_from_memory_size(size_t size)
{
    static const std::vector<std::string> d = { "B", "KiB", "MiB", "GiB", "TiB" };

    size_t i = 0;

    size_t p = 1024;

    while (size / p > 0)
    {
        p *= 1024;
        ++i;
    }
    
    if (i >= d.size() || i == 0)
    {
        return std::to_string(size) + " B";
    }

    p /= 1024; 

    return std::to_string(size / p) + " " + d[i] + " " + (size % p != 0 ? to_string_from_memory_size(size % p) : ""); 
}

int main()
{
	using namespace std;
    constexpr size_t COUNT_THREADS = 32;
	atomic_int barrier(0);
    condition_variable cv;
    size_t sum_size = 0;
    size_t sum_usable_size = 0;
    mutex m;
	vector<thread> threads;
    threads.reserve(COUNT_THREADS);
    for (size_t i = 0; i < COUNT_THREADS; i++)
	{
		threads.emplace_back([&, i]() {
            const size_t size = (i + 1) * 1024 * 1024 * 1;
            sum_size += size;
			volatile void const * const p = new char[size];
            const size_t usable_size = malloc_usable_size((void*)(p));
            sum_usable_size += usable_size; 
            {
                unique_lock<mutex> lk(m);
                std::cout << "[" << i << "] " << "0x" << (void*)p << std::endl; 
                std::cout << "    Size: " << to_string_from_memory_size(size) << std::endl;
                std::cout << "    Usable size: " << to_string_from_memory_size(usable_size) << std::endl;
            }
			barrier++;
			unique_lock<mutex> lk(m);
			cv.wait(lk);
		});
	}

	while (barrier != COUNT_THREADS)
	{
	    this_thread::yield();
	}

    std::cout << "[all]" << std::endl; 
    std::cout << "    Size: " << to_string_from_memory_size(sum_size) << std::endl;
    std::cout << "    Usable size: " << to_string_from_memory_size(sum_usable_size) << std::endl;

	malloc_stats_print(nullptr, nullptr, nullptr);

	const auto pid = ::getpid();
	system((string("gcore ") + std::to_string(pid)).c_str());

	cv.notify_all();
	for (auto& thread : threads)
	{
	    thread.join();
	}

    return 0;
}
